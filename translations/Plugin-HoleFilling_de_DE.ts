<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>HoleFillerControls</name>
    <message>
        <location filename="../holefiller.ui" line="14"/>
        <source>controls</source>
        <translation>Steuerung</translation>
    </message>
    <message>
        <location filename="../holefiller.ui" line="50"/>
        <source>Detect</source>
        <translation>Finden</translation>
    </message>
    <message>
        <location filename="../holefiller.ui" line="57"/>
        <source>Fill selected</source>
        <translation>Auswahl füllen</translation>
    </message>
</context>
<context>
    <name>HoleFillerPlugin</name>
    <message>
        <location filename="../HoleFillerPlugin.cc" line="76"/>
        <source>Hole Filler</source>
        <translation>Löcher Füllen</translation>
    </message>
    <message>
        <location filename="../HoleFillerPlugin.cc" line="91"/>
        <source>Fill all holes from a given Mesh</source>
        <translation>Fülle alle Löcher eines gegebenen Netzes</translation>
    </message>
    <message>
        <location filename="../HoleFillerPlugin.cc" line="94"/>
        <source>Fill a holes from a given Mesh where edgeHandle is on the boundary</source>
        <translation>Fülle alle Löcher eines gegebenen Netzes unter angabe einer Randkante</translation>
    </message>
    <message>
        <location filename="../HoleFillerPlugin.cc" line="122"/>
        <source>Filling holes...</source>
        <translation>Fülle Löcher ...</translation>
    </message>
    <message>
        <location filename="../HoleFillerPlugin.cc" line="122"/>
        <source>Abort</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../HoleFillerPlugin.cc" line="165"/>
        <location filename="../HoleFillerPlugin.cc" line="321"/>
        <source>HoleFilling unsupported for poly meshes</source>
        <translation>Löcher füllen bei Poly Meshes nicht unterstützt</translation>
    </message>
    <message>
        <location filename="../HoleFillerPlugin.cc" line="281"/>
        <source>Error for holeMapping_ vector size</source>
        <translation>Fehlerhafte Vektor Größe bei holeMapping_</translation>
    </message>
    <message>
        <location filename="../HoleFillerPlugin.cc" line="290"/>
        <source>Unable to find object for hole (should not happen!!)</source>
        <translation>Kann kein Objekt zu dem Loch finden! ( Anwendungsfehler! )</translation>
    </message>
    <message>
        <location filename="../HoleFillerPlugin.cc" line="416"/>
        <source>Object</source>
        <translation>Objekt</translation>
    </message>
    <message>
        <location filename="../HoleFillerPlugin.cc" line="416"/>
        <source>Edges</source>
        <translation>Kanten</translation>
    </message>
    <message>
        <location filename="../HoleFillerPlugin.cc" line="416"/>
        <source>Boundary Length</source>
        <translation>Rand Länge</translation>
    </message>
    <message>
        <location filename="../HoleFillerPlugin.cc" line="416"/>
        <source>BB Diagonal</source>
        <translation>Box DIagonale</translation>
    </message>
    <message>
        <location filename="../HoleFillerPlugin.cc" line="567"/>
        <location filename="../HoleFillerPlugin.cc" line="613"/>
        <source>Could not get object from ID.</source>
        <translation>Kann kein Objekt mit der id finden.</translation>
    </message>
    <message>
        <location filename="../HoleFillerPlugin.cc" line="601"/>
        <location filename="../HoleFillerPlugin.cc" line="655"/>
        <source>HoleFilling unsopported for poly meshes.</source>
        <translation>Löcher füllen bei Poly Meshes nicht unterstützt.</translation>
    </message>
    <message>
        <location filename="../HoleFillerPlugin.cc" line="637"/>
        <source>Invalid edge handle.</source>
        <translation>Ungültiges Kanten Handle.</translation>
    </message>
</context>
<context>
    <name>PythonDocHoleFiller</name>
    <message>
        <location filename="../PythonInterface/Python.cc" line="87"/>
        <source>Fills all holes of an object</source>
        <translation>Fülle alle Löcher eines Objektes</translation>
    </message>
    <message>
        <location filename="../PythonInterface/Python.cc" line="88"/>
        <location filename="../PythonInterface/Python.cc" line="92"/>
        <source>Object id</source>
        <translation>Objekt Nummer</translation>
    </message>
    <message>
        <location filename="../PythonInterface/Python.cc" line="91"/>
        <source>Fills a given holes of an object identified by an edge</source>
        <translation>Füllt alle Löcher eines gegebenen Netzes unter Angabe einer Randkante</translation>
    </message>
    <message>
        <location filename="../PythonInterface/Python.cc" line="93"/>
        <source>EdgeHandle id at a boundary of a hole</source>
        <translation>Kanten Nummer am Rande eines Lochs</translation>
    </message>
</context>
</TS>
